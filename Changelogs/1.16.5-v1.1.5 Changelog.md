# 1.16.5-v1.1.5

Mod compat update

## Additions
	- Loot tables from "dungeons_plus" may now be modified with the LootTableLoadEvent
		- "dungeons_plus:chests/vanilla_dungeon" automatically recieves data that would be injected into "minecraft:chests/simple_dungeon"
	- Waystones from the Waystones mod may now generate on top of Towers (configurable)
	- Monster boxes from Quark may now generate inside the Buried Dungeon (configurable)

## Changes
	- Increased the chance of chests generating in the Buried Dungeon
		- Was 50%, Now 70%

## Fixes
	- Fixed the Quark variant chests modual not working on structures